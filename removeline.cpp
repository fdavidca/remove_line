#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/shape.hpp"
#include <opencv2/core/utility.hpp>
#include <math.h>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

RNG rng(12345);

static double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
	double dx1 = pt1.x - pt0.x;
	double dy1 = pt1.y - pt0.y;
	double dx2 = pt2.x - pt0.x;
	double dy2 = pt2.y - pt0.y;
	return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

Point computeCentroid(const cv::Mat &mask) {
    Moments m = moments(mask, true);
    Point center(m.m10/m.m00, m.m01/m.m00);
    return center;
}

Point translateOrigin(Point& point,Point& origin){
	return point-origin;
}

Point projectPoint(Point& pointFromOrigin,Point& oldOrigin){
	double x = pointFromOrigin.x;

	double y = pointFromOrigin.y;
	
	double theta = atan2( y , x);
	double r = sqrt(x*x + y*y);

	double rscaled = r*1.1;

	double newX = rscaled*cos(theta) ;
	double newY = rscaled*sin(theta); 	

	Point result((int)newX,(int)newY);
		

	Point resultTranslated = translateOrigin(result,oldOrigin);
	return resultTranslated;
}



void mserExtractor (const Mat& image, Mat& mserOutMask){
    Ptr<MSER> mserExtractor  = MSER::create();

    vector< vector<cv::Point > > mserContours;
    vector<KeyPoint> mserKeypoint;
    vector<cv::Rect> mserBbox;
    mserExtractor->detectRegions(image, mserContours,  mserBbox);
	int i =0, k = 0;
    for (i=0;i< mserContours.size();i++){
	vector<Point> puntos = mserContours.at(i);
        for (k=0;k<puntos.size();k++){
		Point p = puntos.at(k);
           	mserOutMask.at<uchar>(p.y, p.x) = 255;
        }
    }
}
static vector<Point> simpleContour( const Mat& currentQuery, int n=50 )
{
    vector<vector<Point> > _contoursQuery;
    vector <Point> contoursQuery;
    findContours(currentQuery, _contoursQuery, RETR_LIST, CHAIN_APPROX_NONE);
    for (size_t border=0; border<_contoursQuery.size(); border++)
    {
        for (size_t p=0; p<_contoursQuery[border].size(); p++)
        {
            contoursQuery.push_back( _contoursQuery[border][p] );
        }
    }

    // In case actual number of points is less than n
    int dummy=0;
    for (int add=(int)contoursQuery.size()-1; add<n; add++)
    {
        contoursQuery.push_back(contoursQuery[dummy++]); //adding dummy values
    }

    // Uniformly sampling
    random_shuffle(contoursQuery.begin(), contoursQuery.end());
    vector<Point> cont;
    for (int i=0; i<n; i++)
    {
        cont.push_back(contoursQuery[i]);
    }
    return cont;
}


int main( int argc, char** argv )
{

	VideoCapture cap(1);
        cap.set(CV_CAP_PROP_FRAME_WIDTH,1200);//2304);//1829//1200//800
        cap.set(CV_CAP_PROP_FRAME_HEIGHT,800); //1080//800//600   
        cap.set(CV_CAP_PROP_FPS, 5);
	cv::Ptr <cv::ShapeContextDistanceExtractor> mysc = cv::createShapeContextDistanceExtractor();
//	namedWindow("roi",WINDOW_AUTOSIZE);
    	Mat image,imageOrig,imageHSV;
	Mat imgPatronContorno;
	namedWindow( "Display window", WINDOW_AUTOSIZE );
	namedWindow("adaptive_threshold",WINDOW_AUTOSIZE);
	namedWindow("contornos",WINDOW_AUTOSIZE);
	imgPatronContorno = imread("contorno2.png",IMREAD_GRAYSCALE);	
	for(;;){

    		cap>>image;
		imageOrig = image.clone();
    		if(! image.data )                              // Check for invalid input
    		{
        		cout <<  "Could not open or find the image" << std::endl ;
        		return -1;
    		}
	
		vector<Point> contQuery = simpleContour(imgPatronContorno);    

		Mat imageGrey,imageFiltered;
		bilateralFilter(image,imageFiltered,25,50,12);
		cvtColor(imageFiltered,imageGrey,CV_BGR2GRAY);
		cvtColor(image,imageHSV,CV_BGR2HSV);
		Mat imageDilated;
	
		Mat imageAMT,imageCanny;

		int erosion_size = 1;
		Mat element = getStructuringElement(MORPH_CROSS, Size(2 * erosion_size + 1, 2 * erosion_size + 1),
								Point(erosion_size, erosion_size) );
		dilate(imageGrey,imageDilated,element);
		Canny(imageDilated,imageCanny,63,127,3);

		adaptiveThreshold(imageGrey,imageAMT,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY,7,2);
	
		Mat imageMser = Mat::zeros(imageDilated.size(),CV_8UC1);
		mserExtractor(imageGrey,imageMser);
	
	
		vector< vector<Point> > contours;
		vector<Vec4i> hierarchy;
		findContours(imageMser,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE,Point(0,0));


		RNG rng(12345);
  		Mat drawing = Mat::zeros( imageAMT.size(), CV_8UC3 );
		cout<<"tamanio imageAMT"<<imageAMT.size()<<endl;
	
  		for( int i = 0; i< contours.size(); i++ )
     		{
			Scalar color = Scalar(255,255,255);
			//Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
			if(contours[i].size()<50)
				continue;
               		if( arcLength(Mat(contours[i]),false ) < 100 || arcLength( Mat(contours[i]), false ) > 1000 )continue;
		
			double arco = arcLength(Mat(contours[i]),false);
			double area = contourArea(contours[i],false);
			//		cout<<"longitud dividido area"<<arco/area <<endl;
			double radio = arco/area;
			if(radio < 0.1 && area > 800){
				Rect rect = boundingRect(contours.at(i));
				int rectX = rect.x;
				int rectY = rect.y;
				Point origenGlobal = Point(0,0) -Point(rectX,rectY);
				cout<<"Origen Global "<<origenGlobal<<endl;
				Mat matAux = Mat::zeros(imageAMT.size(),CV_8UC3);		
				drawContours( matAux, contours, i, color, -1, 4, hierarchy, 0, Point(0,0) );
				Mat ROI = matAux(rect);
				Mat roiGrey;
				cvtColor(ROI,roiGrey,COLOR_RGB2GRAY);
				vector< vector<Point> > contornosAgrandados; 
				vector<Point> contii = simpleContour(roiGrey);
       				float dis = mysc->computeDistance( contQuery, contii );
				cout<<"distancia a contorno"<<dis<<endl;
				if(dis < 0.25){
					Point centroide = computeCentroid(roiGrey);
					drawContours( image, contours, i,Scalar(0,255,0), 1, 4, hierarchy, 0, Point(0,0) );
				
					Point translated = translateOrigin(centroide,centroide);
					cout<<"centroide original "<<centroide<<"centroide translated "<<translated<<endl;
					vector<Point> contornoAgrandado ;
					Point origenTransladado = Point(0,0) - centroide;
					cout<<"origen transladado"<<origenTransladado<<endl;
					double intensidadAnterior = 0.0;	
					for(int k=0;k<contii.size();k++){
						Point puntoTransladado = translateOrigin(contii[k],centroide);
//						cout<<"pto orig "<<contii[k]<<"pto transladado "<<puntoTransladado<<endl;
						Point proyectado = projectPoint(puntoTransladado,origenTransladado);
						Point transladado2 = translateOrigin(proyectado,origenGlobal);
//						cout<<"Transladado 2"<<transladado2<<endl;
						contornoAgrandado.push_back(transladado2);
						int x1 = (int)(transladado2.x+0.5);
						int y1 = (int)(transladado2.y+0.5);
									
						if(waitKey(30) >= 0 ){
							break;
						}	
//						cout<<" proyectado" <<proyectado<<endl;
					}
					for(int j = 0 ; j < contornoAgrandado.size();j++){
						circle(image,contornoAgrandado[j],0,Scalar(255,255,255));
					}
					contornosAgrandados.push_back( contornoAgrandado );				
				}//end if (dis < 0.75)
			
				
				

				if(contornosAgrandados.size() > 0){
			//			drawContours( image, contornosAgrandados , -1 , Scalar(255,255,255) , 1,4);
				
				}				

				if(waitKey(30) >= 0 ){
						break;
				}		
			}//end	if(radio < 0.75 && area > 800)
			if(waitKey(30) >= 0 ){
						break;
			}
     		}// end	for( int i = 0; i< contours.size(); i++ )
	
	if(waitKey(30) >= 0)
	{
		break;
	}
   	
	waitKey(120);		
        imshow( "Display window", image );                   // Show our image inside it.
	imshow("adaptive_threshold",imageGrey);
	imshow("contornos",imageMser);
        
	}//end for principal
        waitKey(0);                                   // Wait for a keystroke in the window
        return 0;
}
